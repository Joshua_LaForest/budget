from tax_calculator import taxes
# f"You took home {taxes_rec(income, i)} and you paid {income -taxes_rec(income, i)} out of {income}"
def budget():
    year = 1
    months = 12
    weeks = 52
    income = int(input("How much did you get payed this year? $"))
    income_after_tax = taxes(income)
    yearly_tax = income - income_after_tax
    tax_per_month = yearly_tax/months
    income_per_month = income_after_tax/months
    
    rent = int(input("how much will you be paying for rent?"))
    other_monthly_expenses = int(input("how much are your other monthly expenses? food, insurance, subscriptions?"))
    left_over_monthly = income_per_month - rent - other_monthly_expenses
    left_over_yearly = income_after_tax - (rent*12) - (other_monthly_expenses*12)
    return (f"""you will make ${income_after_tax} after getting taxed ${yearly_tax}
you will make ${income_per_month} every month after a monthly tax of ${tax_per_month}
you will take home ${left_over_monthly} after rent, and other monthly expenses which total ${rent+other_monthly_expenses}
you will take home ${left_over_yearly} after one year.
        """)
print(budget())